import { calc, stringToItem } from './calc'

describe('test', () => {
  test('001000 to 110000', () => {
    expect(
      calc({ input: stringToItem('001000'), target: stringToItem('110000') })
    ).toBe(17) // 17
  })
  test('101010 to 010101', () => {
    expect(
      calc({ input: stringToItem('101010'), target: stringToItem('010101') })
    ).toBe(26) // 26
  })
  test('000 to 111', () => {
    expect(
      calc({ input: stringToItem('000'), target: stringToItem('111') })
    ).toBe(5) //5
  })
  test('100 to 111', () => {
    expect(
      calc({ input: stringToItem('100'), target: stringToItem('111') })
    ).toBe(2) //2
  })
  test('001 to 111', () => {
    expect(
      calc({ input: stringToItem('001'), target: stringToItem('111') })
    ).toBe(4) //4
  })
  test('1101 to 0100', () => {
    expect(
      calc({ input: stringToItem('1101'), target: stringToItem('0100') })
    ).toBe(2)
  })
  test('big one ! 11001001000 to 10000110011', () => {
    expect(
      calc({
        input: stringToItem('11001001000'),
        target: stringToItem('10000110011'),
      })
    ).toBe(877)
  }) // 877
})
