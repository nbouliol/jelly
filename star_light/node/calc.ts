import { Input, Item } from './types'

export function stringToItem(s: string): Item[] {
  return s.split('').map((x) => parseInt(x) as Item)
}

const getKey = (input: Item[]): string =>
  input.reduce((acc, curr) => acc + curr.toString(), '')

export function calc({ input, target }: Input): number {
  const len = input.length

  if (len === 1) {
    return input[0] !== target[0] ? 1 : 0
  }

  const operationsList: { [K: string]: number } = {}
  let operations = 0

  if (input[0] !== target[0]) {
    let key = getKey(input)
    operationsList[key] = operations

    while (true) {
      // iterate until we find a 1 and invert its left item
      for (let i = len - 1; i > 0; i--) {
        if (input[i] === 1) {
          input[i - 1] = input[i - 1] === 0 ? 1 : 0
          operations += 1

          key = getKey(input)
          // store them
          if (operationsList[key] !== undefined) {
            operations = Math.min(operations, operationsList[key])
          } else {
            operationsList[key] = operations
          }

          break
        }
      }

      // no need to continue if they are equal
      if (input[0] === target[0]) {
        break
      }

      // invert last item
      input[len - 1] = input[len - 1] === 0 ? 1 : 0
      operations += 1

      // store them
      key = getKey(input)

      if (operationsList[key] !== undefined) {
        operations = Math.min(operations, operationsList[key])
      } else {
        operationsList[key] = operations
      }
    }
  }

  return operations + calc({ input: input.slice(1), target: target.slice(1) })
}
