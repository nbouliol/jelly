export type Item = 0 | 1

export interface Input {
  input: Item[]
  target: Item[]
}
