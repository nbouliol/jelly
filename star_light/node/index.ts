import { calc, stringToItem } from './calc'
import { Input } from './types'

var readline = require('readline')

const getInput = async (): Promise<Input> => {
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })

  return new Promise((resolve) => {
    const input: Partial<Input> = {}
    let i = 0

    rl.on('line', function (line: string) {
      if (i == 0) {
        input.input = stringToItem(line)
      }
      if (i == 1) {
        input.target = stringToItem(line)
        console.log('')
        rl.close()
        resolve(input as Input)
      }
      i++
    })
  })
}

;(async function main() {
  const inputs = await getInput()
  return calc(inputs)
})()
  .then((r) => console.log(r))
  .catch((err) => console.error(err))
