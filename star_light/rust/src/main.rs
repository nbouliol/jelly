use std::cmp::min;
use std::collections::HashMap;
use std::fmt::Write;
use std::io;

macro_rules! parse_input {
    ($x:expr) => {{
        let mut v = Vec::new();
        for e in $x.trim().chars() {
            v.push(e.to_digit(10).unwrap());
        }
        v
    }};
}

fn switch(value: u32) -> u32 {
    if value == 0 {
        1
    } else {
        0
    }
}

// https://users.rust-lang.org/t/how-do-i-convert-vec-of-i32-to-string/18669/10
fn join(a: &[u32]) -> String {
    a.iter().fold(String::new(), |mut s, &n| {
        write!(s, "{}", n).ok();
        s
    })
}

fn calc(lights: &mut [u32], target: &[u32]) -> u32 {
    let len = lights.len();
    if len == 1 {
        return if lights[0] == target[0] { 0 } else { 1 };
    }
    let mut operations = 0;
    let mut operations_map: HashMap<String, u32> = HashMap::new();

    if lights[0] != target[0] {
        let key = join(&lights);
        operations_map.insert(key, operations);

        loop {
            for index in (1..len).rev() {
                // switch first 1's left

                if lights[index] == 1 {
                    lights[index - 1] = switch(lights[index - 1]);
                    operations += 1;

                    let key = join(&lights);
                    if operations_map.contains_key(&key) {
                        operations = min(operations, *operations_map.get(&key).unwrap());
                    } else {
                        operations_map.insert(key, operations);
                    }

                    break;
                }
            }

            if lights[0] == target[0] {
                break;
            }

            // switch last element
            lights[len - 1] = switch(lights[len - 1]);
            operations += 1;

            let key = join(&lights);
            if operations_map.contains_key(&key) {
                operations = min(operations, *operations_map.get(&key).unwrap());
            } else {
                operations_map.insert(key, operations);
            }
        }
    }
    operations + calc(&mut lights[1..], &target[1..])
}

fn main() {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let start = input_line.trim_end().to_string();
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let target = parse_input!(input_line.trim_end().to_string());

    let mut lights = parse_input!(start);

    let operations: u32 = calc(&mut lights, &target);

    println!("{}", operations);
}

#[cfg(test)]
mod test {
    // use super::*;

    #[test]
    fn switch() {
        assert!(crate::switch(0) == 1);
        assert!(crate::switch(1) == 0);
    }

    #[test]
    fn input1() {
        assert!(crate::calc(&mut vec![1, 1, 0, 1], &vec![0, 1, 0, 0]) == 2)
    }

    #[test]
    fn input2() {
        assert!(crate::calc(&mut vec![1, 0, 1, 0, 1, 0], &vec![0, 1, 0, 1, 0, 1]) == 26)
    }

    #[test]
    fn input3() {
        assert!(
            crate::calc(
                &mut vec![1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0],
                &vec![1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1]
            ) == 877
        )
    }
}
