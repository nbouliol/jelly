use std::fmt;
use std::io;
use std::result;
use thiserror::Error;

macro_rules! parse_input {
    ($x:expr, $t:ident) => {
        $x.trim().parse::<$t>().unwrap()
    };
}

pub type Result<T> = result::Result<T, StickyError>;

fn get_lines(size: usize) -> Result<(Vec<Vec<char>>, Vec<Vec<Path>>)> {
    let mut lines = Vec::new();
    let mut paths = Vec::new();

    for x in 0..size as usize {
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line)?;

        let mut line = input_line
            .trim_end()
            .chars()
            .into_iter()
            .collect::<Vec<char>>();

        // get first and last lines
        if x == 0 || x == (size - 1) as usize {
            line.retain(|c| !c.is_whitespace());
            lines.push(line);
            continue;
        }

        line.dedup();

        paths.push(get_line_paths(&line, x));
    }
    return Ok((lines, paths));
}

fn get_line_paths(line: &[char], row: usize) -> Vec<Path> {
    let mut line_paths = Vec::new();
    let len = line.len();

    for (y, c) in line.iter().enumerate() {
        if *c == '|' {
            // line_paths.push(get_path(&line, y, row, len))
            let mut to_left = false;
            let mut to_right = false;

            if y > 0 && line[y - 1] == '-' {
                to_left = true;
            }
            if y + 1 < len as usize && line[y + 1] == '-' {
                to_right = true;
            }

            if y == 0 {
                line_paths.push(Path::new(y, row, to_left, to_right));
            } else {
                line_paths.push(Path::new(y / 2, row, to_left, to_right))
            }
        }
    }

    line_paths
}

fn calc(lines: &[Vec<char>], paths: &[Vec<Path>], height: usize) -> Vec<(char, char)> {
    let mut results = Vec::new();
    let top = lines.first().expect("Top line is empty");
    let bottom = lines.last().expect("Bottom line is empty");

    for (i, c) in top.iter().enumerate() {
        let mut index = i;
        // -2 because I removed width/height, top, bottom lines and x..y is not inclusive
        for j in 0..(height - 2) {
            let path = &paths[j];
            let p = path
                .into_iter()
                .find(|&x| x.col == index)
                .expect("did not find any matching index");

            if p.to_left {
                index -= 1;
            }
            if p.to_right {
                index += 1;
            }
        }
        results.push((*c, bottom[index]));
    }

    return results;
}

fn main() -> Result<()> {
    let mut input_line = String::new();

    io::stdin().read_line(&mut input_line).unwrap();
    let inputs = input_line.split(" ").collect::<Vec<_>>();

    let h = parse_input!(inputs[1], usize);
    let (lines, paths) = get_lines(h)?;

    let results = calc(&lines, &paths, h);

    for pair in results {
        println!("{}{}", pair.0, pair.1);
    }

    Ok(())
}

#[derive(Debug, PartialEq)]
pub struct Path {
    to_left: bool,
    to_right: bool,
    row: usize,
    col: usize,
}

impl Path {
    pub fn new(col: usize, row: usize, to_left: bool, to_right: bool) -> Self {
        Self {
            col,
            row,
            to_left,
            to_right,
        }
    }
}

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Path [ row {} - col {} - to_left {} - to_right {} ]",
            self.row, self.col, self.to_left, self.to_right
        )
    }
}

#[derive(Error, Debug)]
pub enum StickyError {
    #[error("data store disconnected")]
    IO(#[from] io::Error),
    #[error("unknown error")]
    Unknown,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_path() {
        let row = 1;
        let line = vec!['|', ' ', '|', ' ', '|'];

        let out = vec![
            Path::new(0, row, false, false),
            Path::new(1, row, false, false),
            Path::new(2, row, false, false),
        ];

        assert!(out == get_line_paths(&line, row));

        let line = vec!['|', '-', '|', ' ', '|'];
        let out = vec![
            Path::new(0, row, false, true),
            Path::new(1, row, true, false),
            Path::new(2, row, false, false),
        ];

        assert!(out == get_line_paths(&line, row));
    }
}
